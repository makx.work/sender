<?php

declare(strict_types=1);

namespace Sender\UI\Cli\Command;

use Sender\Application\Validation\PhoneNumber\PhoneNumberValidatorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ValidatorCommand extends Command
{
    private $phoneNumberValidator;

    public function __construct(PhoneNumberValidatorInterface $phoneNumberValidator)
    {
        $this->phoneNumberValidator = $phoneNumberValidator;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('number:validator')
            ->setDescription('Check number');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $number = $helper->ask($input, $output, new Question('Phone Number: '));

        $this->phoneNumberValidator->validate($number);

        $output->writeln($number);

        $output->writeln('<info>Done!</info>');
    }
}
