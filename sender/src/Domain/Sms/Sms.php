<?php

declare(strict_types=1);

namespace Sender\Domain\Sms;

use Sender\Domain\Message\AbstractMessage;
use Sender\Domain\PhoneNumber;

final class Sms extends AbstractMessage
{
    private $phoneNumber;

    public function __construct(string $content, PhoneNumber $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        $this->status = self::STATUS_NEW;

        parent::__construct($content);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getPhoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }
}
