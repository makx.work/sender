<?php

declare(strict_types=1);

namespace Sender\Domain\Sms;

use Sender\Domain\PhoneNumber;
use Sender\Application\Messenger\MessageDto;

final class SmsCreator
{
    public static function create(MessageDto $message)
    {
        $phoneNumber = PhoneNumber::fromString($message->phone);

        return new Sms($message->content, $phoneNumber);
    }
}
