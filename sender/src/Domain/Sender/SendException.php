<?php

declare(strict_types=1);

namespace Sender\Domain\Sender;

use DomainException;

final class SendException extends DomainException
{
    public static function create(string $message): self
    {
        return new self($message);
    }
}
