<?php

declare(strict_types=1);

namespace Sender\Domain\Sender;

use Sender\Domain\Message\Message;

interface SenderInterface
{
    /**
     * @param Message $message
     * @throws SendException
     */
    public function send(Message $message);
}
