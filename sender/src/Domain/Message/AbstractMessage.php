<?php

declare(strict_types=1);

namespace Sender\Domain\Message;

abstract class AbstractMessage implements Message
{
    public const STATUS_NEW = 'new';
    public const STATUS_PROCESS = 'process';
    public const STATUS_DONE = 'done';

    protected $content;
    protected $status;

    public function getContent(): string
    {
        return $this->content;
    }

    protected function __construct(string $content)
    {
        $this->content = $content;
    }

    public function process()
    {
        $this->status = self::STATUS_PROCESS;
    }

    public function done()
    {
        $this->status = self::STATUS_DONE;
    }
}
