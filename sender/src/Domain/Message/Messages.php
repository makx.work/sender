<?php

declare(strict_types=1);

namespace Sender\Domain\Message;

final class Messages
{
    public const READY = 'READY';
    public const KO    = 'KO';
    public const DONE  = 'DONE';
    public const INPROGRESS  = 'INPROGRESS';
}
