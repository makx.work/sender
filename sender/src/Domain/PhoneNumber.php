<?php

declare(strict_types=1);

namespace Sender\Domain;

final class PhoneNumber
{
    private $number;

    public static function fromString(string $value)
    {
        $self = new self();
        $self->number = $value;

        return $self;
    }

    public function getNumber()
    {
        return $this->number;
    }

    private function __construct()
    {
    }
}
