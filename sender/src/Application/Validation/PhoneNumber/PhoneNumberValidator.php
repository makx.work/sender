<?php

declare(strict_types=1);

namespace Sender\Application\Validation\PhoneNumber;

use Sender\Application\Validation\PhoneNumber\Exception\PhoneNumberInvalidException;
use Sender\Application\Validation\PhoneNumber\Exception\PhoneNumberCouldNotBeParsedException;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

final class PhoneNumberValidator implements PhoneNumberValidatorInterface
{
    /**
     * @var PhoneNumberUtil
     */
    private $phoneNumberUtil;

    /**
     * @var string
     */
    private $countryCode;

    public function __construct(PhoneNumberUtil $phoneNumberUtil, string $countryCode)
    {
        $this->phoneNumberUtil = $phoneNumberUtil;
        $this->countryCode = $countryCode;
    }

    public function validate(string $phoneNumber, string $countryCode = null): void
    {
        try {
            $number = $this->phoneNumberUtil->parse($phoneNumber, $countryCode ?? $this->countryCode);
        } catch (NumberParseException $exception) {
            throw PhoneNumberCouldNotBeParsedException::create($phoneNumber);
        }
        if (!$this->phoneNumberUtil->isValidNumber($number)) {
            throw PhoneNumberInvalidException::create($phoneNumber);
        }
    }
}
