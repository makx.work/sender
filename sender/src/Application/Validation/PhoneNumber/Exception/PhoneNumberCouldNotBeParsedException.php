<?php

declare(strict_types=1);

namespace Sender\Application\Validation\PhoneNumber\Exception;

use DomainException;

final class PhoneNumberCouldNotBeParsedException extends DomainException implements PhoneNumberException
{
    public static function create(string $phoneNumber): self
    {
        return new self(sprintf('Phone number %s could not be parsed', $phoneNumber));
    }
}
