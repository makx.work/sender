<?php

declare(strict_types=1);

namespace Sender\Application\Validation\PhoneNumber\Exception;

use Throwable;

interface PhoneNumberException extends Throwable
{

}
