<?php

declare(strict_types=1);

namespace Sender\Application\Validation\PhoneNumber;

use Sender\Application\Validation\PhoneNumber\Exception\PhoneNumberException;

interface PhoneNumberValidatorInterface
{
    /**
     * @param string $phoneNumber
     * @param string $countryCode
     *
     * @throws PhoneNumberException
     */
    public function validate(string $phoneNumber, string $countryCode = null): void;
}
