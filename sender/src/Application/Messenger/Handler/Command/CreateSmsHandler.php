<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Handler\Command;

use Sender\Application\Messenger\Message\Command\CreateSms;
use Psr\Log\LoggerInterface;
use Sender\Application\Messenger\MessageDto;
use Sender\Domain\Sms\Sms;
use Sender\Application\Messenger\Message\Command\SendSms;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateSmsHandler implements MessageHandlerInterface
{
    private $smsProcessLogger;
    private $serializer;
    private $commandBus;

    public function __construct(
        LoggerInterface $smsProcessLogger,
        SerializerInterface $serializer,
        MessageBusInterface $commandBus
    ) {
        $this->smsProcessLogger = $smsProcessLogger;
        $this->serializer = $serializer;
        $this->commandBus = $commandBus;
    }

    public function __invoke(CreateSms $message)
    {
        /** @var MessageDto $messageDto */
        $messageDto = $message->getMessageDto();

        $this->smsProcessLogger->info(sprintf(
            'SMS %s: %s - %s',
            strtoupper(Sms::STATUS_NEW),
            $messageDto->scheduleId,
            $messageDto->phone
        ));

        $this->commandBus->dispatch(new SendSms($messageDto));
    }
}
