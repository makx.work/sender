<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Handler\Command;

use Psr\Log\LoggerInterface;
use Sender\Application\Messenger\Message\Command\SendSms;
use Sender\Application\Messenger\Message\Event\SmsSentEvent;
use Sender\Application\Messenger\MessageDto;
use Sender\Domain\Sender\SenderInterface;
use Sender\Domain\Sender\SendException;
use Sender\Domain\Sms\Sms;
use Sender\Domain\Sms\SmsCreator;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SendSmsHandler implements MessageHandlerInterface
{
    private $smsProcessLogger;
    private $serializer;
    private $eventBus;
    private $sender;

    public function __construct(
        LoggerInterface $smsProcessLogger,
        SerializerInterface $serializer,
        MessageBusInterface $eventBus,
        SenderInterface $sender
    ) {
        $this->smsProcessLogger = $smsProcessLogger;
        $this->serializer = $serializer;
        $this->eventBus = $eventBus;
        $this->sender = $sender;
    }

    public function __invoke(SendSms $sendSms)
    {
        /** @var MessageDto $messageDto */
        $messageDto = $sendSms->getMessageDto();

        /** @var Sms $sms */
        $sms = SmsCreator::create($messageDto);
        $sms->process();

        try {
            $this->sender->send($sms);
        } catch (SendException $exception) {
            // смотреть в чем причина возможно нужна повторная отправка
        }

        $this->smsProcessLogger->info(sprintf(
            'SMS %s: %s - %s',
            strtoupper(Sms::STATUS_PROCESS),
            $messageDto->scheduleId,
            $messageDto->phone
        ));

        $this->eventBus->dispatch(new SmsSentEvent($messageDto));
    }
}
