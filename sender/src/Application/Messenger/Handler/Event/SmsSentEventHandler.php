<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Handler\Event;

use Psr\Log\LoggerInterface;
use Sender\Application\Messenger\Message\Event\SmsSentEvent;
use Sender\Application\Messenger\MessageDto;
use Sender\Domain\Sms\Sms;
use Symfony\Component\Messenger\Envelope;
use Sender\Application\Messenger\BackStatus;
use Sender\Application\Messenger\Message\Command\SendBackStatus;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class SmsSentEventHandler implements MessageHandlerInterface
{
    private $smsProcessLogger;
    private $commandBus;
    private $serializer;

    public function __construct(
        LoggerInterface $smsProcessLogger,
        SerializerInterface $serializer,
        MessageBusInterface $commandBus
    ) {
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
        $this->smsProcessLogger = $smsProcessLogger;
    }

    public function __invoke(SmsSentEvent $smsSentEvent)
    {
        /** @var MessageDto $sms */
        $messageDto = $smsSentEvent->getMessageDto();

        $this->smsProcessLogger->info(sprintf(
            'SMS %s: %s - %s',
            strtoupper(Sms::STATUS_DONE),
            $messageDto->scheduleId,
            $messageDto->phone
        ));

        $backStatus = new SendBackStatus();
        $backStatus->scheduleId = $messageDto->scheduleId;

        $backStatus->status = (rand(0, 10) < 7) ? BackStatus::FAIL : BackStatus::DONE;

        $this->commandBus->dispatch($backStatus);
    }
}
