<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Handler\Event;

use Psr\Log\LoggerInterface;
use Sender\Application\Messenger\Message\Command\CreateSms;
use Sender\Application\Messenger\MessageDto;
use Symfony\Component\Messenger\MessageBusInterface;
use Sender\Application\Messenger\Message\Event\ReceiveMessageEvent;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class ReceiveMessageEventHandler implements MessageHandlerInterface
{
    private $messageReceivedLogger;
    private $commandBus;
    private $serializer;

    public function __construct(
        LoggerInterface $messageReceivedLogger,
        SerializerInterface $serializer,
        MessageBusInterface $commandBus
    ) {
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
        $this->messageReceivedLogger = $messageReceivedLogger;
    }

    public function __invoke(ReceiveMessageEvent $message)
    {
        /** @var MessageDto $messageInfo */
        $messageInfo = $message->getMessage();

        $this->messageReceivedLogger->info(sprintf(
            'New message received: %s',
            $this->serializer->serialize($messageInfo, 'json')
        ));

        $this->commandBus->dispatch(new CreateSms($messageInfo));
    }
}
