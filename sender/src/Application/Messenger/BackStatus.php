<?php

declare(strict_types=1);

namespace Sender\Application\Messenger;

final class BackStatus
{
    public const DONE = 'DONE';
    public const FAIL = 'FAIL';
}
