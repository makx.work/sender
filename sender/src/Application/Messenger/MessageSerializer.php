<?php

declare(strict_types=1);

namespace Sender\Application\Messenger;

use Sender\Application\Messenger\Message\Event\ReceiveMessageEvent;
use Symfony\Component\Messenger\Stamp\BusNameStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface as TransportSerializer;

final class MessageSerializer implements TransportSerializer
{
    private $serializer;
    private $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function decode(array $encodedEnvelope): Envelope
    {
        /** @var MessageDto $command */
        $command = $this->serializer->deserialize($encodedEnvelope['body'], MessageDto::class, 'json');
        $violations = $this->validator->validate($command);

        if (\count($violations)) {
            throw new MessageDecodingFailedException($this->serializer->serialize($violations, 'json'));
        }

        return $this->createMessage($command);
    }

    public function encode(Envelope $envelope): array
    {
        throw new \Exception('Transport not support sending message');
    }

    /**
     * @param MessageDto $message
     * @return Envelope
     */
    private function createMessage(MessageDto $message): Envelope
    {
        $message = new ReceiveMessageEvent($message);
        $envelope = new Envelope($message);

        $envelope->with(new BusNameStamp('event.bus'));

        return $envelope;
    }
}
