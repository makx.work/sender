<?php

declare(strict_types=1);

namespace Sender\Application\Messenger;

use Sender\Domain\Message\Messages;
use Symfony\Component\Validator\Constraints as Assert;

final class MessageDto
{
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $scheduleId;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $phone;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var string
     */
    public $content = 'Default content';

    /**
     * @var string
     * @Assert\Choice(
     *      choices = {
     *          Messages::READY: "READY",
     *          Messages::KO: "KO",
     *          Messages::DONE: "DONE",
     *          Messages::INPROGRESS: "INPROGRESS",
     *      },
     *     message = "Not a valid status."
     * )
     */
    public $status;
}
