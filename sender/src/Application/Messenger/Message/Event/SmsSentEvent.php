<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Message\Event;

use Sender\Application\Messenger\MessageDto;

final class SmsSentEvent
{
    private $messageDto;

    public function __construct(MessageDto $messageDto)
    {
        $this->messageDto = $messageDto;
    }

    public function getMessageDto()
    {
        return $this->messageDto;
    }
}
