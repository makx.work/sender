<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Message\Event;

use Sender\Application\Messenger\MessageDto;

final class ReceiveMessageEvent
{
    private $message;

    public function __construct(MessageDto $message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
