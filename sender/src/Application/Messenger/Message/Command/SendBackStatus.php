<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Message\Command;

final class SendBackStatus
{
    /**
     * @var int
     */
    public $scheduleId;

    /**
     * @var string
     */
    public $status;
}
