<?php

declare(strict_types=1);

namespace Sender\Application\Messenger\Message\Command;

use Sender\Application\Messenger\MessageDto;

final class CreateSms
{
    private $messageDto;

    public function getMessageDto(): MessageDto
    {
        return $this->messageDto;
    }

    public function __construct(MessageDto $messageDto)
    {
        $this->messageDto = $messageDto;
    }
}
