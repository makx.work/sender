<?php declare(strict_types=1);

namespace Sender\Tests\Application\Messenger\Handler\Command;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Sender\Application\Messenger\Message\Command\SendSms;
use Psr\Log\LoggerInterface;
use Sender\Application\Messenger\Handler\Command\CreateSmsHandler;
use Sender\Application\Messenger\Message\Command\CreateSms;
use Sender\Application\Messenger\MessageDto;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

class CreateSmsHandlerTest extends TestCase
{
    /**
     * @var SerializerInterface|MockObject
     */
    private $serializer;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var MessageBusInterface|MockObject
     */
    private $bus;

    /**
     * @var CreateSmsHandler
     */
    private $handler;

    protected function setUp()
    {
        $this->serializer = $this->getMockBuilder(SerializerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->bus = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()->getMock();


        $this->handler = new CreateSmsHandler(
            $this->logger,
            $this->serializer,
            $this->bus
        );
    }

    public function testCorrectDispatchMessage()
    {
        $message = new CreateSms(new MessageDto());

        $this->bus->expects(self::exactly(1))
            ->method('dispatch')
            ->withConsecutive(
                [ self::isInstanceOf(SendSms::class) ]
            )
        ->willReturn(new Envelope($message));

        $handler = $this->handler;
        $handler($message);
    }
}