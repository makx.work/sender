<?php declare(strict_types=1);

namespace Sender\Tests\Application\Messenger\Handler\Command;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Sender\Application\Messenger\Handler\Command\SendSmsHandler;
use Sender\Application\Messenger\Message\Command\SendSms;
use Sender\Application\Messenger\Message\Event\SmsSentEvent;
use Sender\Application\Messenger\MessageDto;
use Sender\Domain\Message\Messages;
use Sender\Domain\Sender\SenderInterface;
use Sender\Domain\Sms\SmsCreator;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

class SendSmsHandlerTest extends TestCase
{
    /**
     * @var SerializerInterface|MockObject
     */
    private $serializer;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var MessageBusInterface|MockObject
     */
    private $bus;

    /**
     * @var SenderInterface|MockObject
     */
    private $sender;

    /**
     * @var SendSmsHandler
     */
    private $handler;

    protected function setUp()
    {
        $this->serializer = $this->getMockBuilder(SerializerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->bus = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()->getMock();


        $this->sender = $this->getMockBuilder(SenderInterface::class)
            ->disableOriginalConstructor()->getMock();


        $this->handler = new SendSmsHandler(
            $this->logger,
            $this->serializer,
            $this->bus,
            $this->sender
        );
    }

    public function testCorrectDispatchMessage()
    {
        $message = new SendSms($this->createMessageDto());

        $this->bus->expects(self::exactly(1))
            ->method('dispatch')
            ->withConsecutive(
                [ self::isInstanceOf(SmsSentEvent::class) ]
            )
            ->willReturn(new Envelope($message));
        
        $handler = $this->handler;
        $handler($message);
    }

    public function testWhenHandlerDispatchSenderWasCallMethodSend()
    {
        $message = new SendSms($dto = $this->createMessageDto());

        $this->bus->expects(self::exactly(1))
            ->method('dispatch')
            ->withConsecutive(
                [ self::isInstanceOf(SmsSentEvent::class) ]
            )
            ->willReturn(new Envelope($message));

        $sms = SmsCreator::create($dto);
        $sms->process();

        $this->sender->expects($this->once())
            ->method('send')
            ->with($sms);

        $handler = $this->handler;
        $handler($message);
    }

    private function createMessageDto()
    {
        $dto = new MessageDto();
        $dto->scheduleId = 1;
        $dto->status = Messages::INPROGRESS;

        $dto->phone = '444-666-444';
        $dto->content = 'Content';
        $dto->name = 'Name';

        return $dto;
    }
}