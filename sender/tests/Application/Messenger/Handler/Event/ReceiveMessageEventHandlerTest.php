<?php declare(strict_types=1);

namespace Sender\Tests\Application\Messenger\Handler\Event;

use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Sender\Application\Messenger\Handler\Event\ReceiveMessageEventHandler;
use Sender\Application\Messenger\Message\Command\CreateSms;
use Sender\Application\Messenger\Message\Event\ReceiveMessageEvent;
use Sender\Application\Messenger\MessageDto;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ReceiveMessageEventHandlerTest extends TestCase
{
    /**
     * @var SerializerInterface|MockObject
     */
    private $serializer;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var MessageBusInterface|MockObject
     */
    private $bus;

    /**
     * @var ReceiveMessageEventHandler
     */
    private $handler;

    protected function setUp()
    {
        $this->serializer = $this->getMockBuilder(SerializerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->bus = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()->getMock();


        $this->handler = new ReceiveMessageEventHandler(
            $this->logger,
            $this->serializer,
            $this->bus
        );
    }

    public function testCorrectDispatchMessage()
    {
        $message = new ReceiveMessageEvent(new MessageDto());

        $this->bus->expects(self::exactly(1))
            ->method('dispatch')
            ->withConsecutive(
                [ self::isInstanceOf(CreateSms::class) ]
            )
            ->willReturn(new Envelope($message));

        $handler = $this->handler;
        $handler($message);
    }
}