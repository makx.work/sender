<?php declare(strict_types=1);

namespace Sender\Tests\Domain;

use PHPUnit\Framework\TestCase;
use Sender\Domain\PhoneNumber;
use Sender\Domain\Sms\Sms;

class SmsTest extends TestCase
{
    public function testCreate(): void
    {
        $phoneNumber = PhoneNumber::fromString($number = '0998778587');

        $sms = new Sms($content = 'some text', $phoneNumber);

        $this->assertEquals($content, $sms->getContent());
        $this->assertEquals($phoneNumber, $sms->getPhoneNumber());

        $this->assertEquals(SMS::STATUS_NEW, $sms->getStatus());
    }

    public function testStatus(): void
    {
        $phoneNumber = PhoneNumber::fromString($number = '0998778587');
        $sms = new Sms($content = 'some text', $phoneNumber);

        $this->assertEquals(SMS::STATUS_NEW, $sms->getStatus());

        $sms->process();
        $this->assertEquals(SMS::STATUS_PROCESS, $sms->getStatus());

        $sms->done();
        $this->assertEquals(SMS::STATUS_DONE, $sms->getStatus());
    }
}