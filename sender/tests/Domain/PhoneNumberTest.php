<?php declare(strict_types=1);

namespace Sender\Tests\Domain;

use PHPUnit\Framework\TestCase;
use Sender\Domain\PhoneNumber;

class PhoneNumberTest extends TestCase
{
    public function testCreate(): void
    {
        $phoneNumber = PhoneNumber::fromString($number = '0998778587');

        $this->assertEquals($number, $phoneNumber->getNumber());
    }
}