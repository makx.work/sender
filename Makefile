up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up sender-init sender-test
test: sender-test
lint: sender-lint

build: build-prod
push: push-prod

build-prod:
	docker --log-level=debug build --pull --file=sender/docker/prod/php-cli.docker --tag=gcr.io/${PROJECT_ID}/sender-php-cli:v${IMAGE_TAG} sender

push-prod:
	docker push gcr.io/${PROJECT_ID}/sender-php-cli:v${IMAGE_TAG}

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

sender-init: sender-composer-install

sender-composer-install:
	docker-compose run --rm sender-php-cli composer install

sender-test:
	docker-compose run --rm sender-php-cli composer test

sender-lint:
	docker-compose run --rm sender-php-cli composer lint
	docker-compose run --rm sender-php-cli composer cs-check